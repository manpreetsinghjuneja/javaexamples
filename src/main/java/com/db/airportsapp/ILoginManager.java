package com.db.airportsapp;

public interface ILoginManager {

    public boolean isUserAuthenticated(String username);

    public boolean doesUserExist(String username);
}
