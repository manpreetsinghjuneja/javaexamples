package com.db.tests;

import com.db.airportsapp.Airport;
import com.db.airportsapp.AirportAppFactory;
import com.db.airportsapp.IAirportsApp;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AirportAppTest {
    @Test
    public void testFindAirportByCode() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode("6523");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByCodeBadArgument() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode(null);
    }

    @Test
    public void testFindAirportByName() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName("Lowell Field");
        assertFalse(actualAirports.isEmpty());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByNameBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName(null);
    }

    @Test
    public void testFindAirportByLatitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude("38.704022");
        assertFalse(actualAirports.isEmpty());
        for (Airport airport : actualAirports) {
            System.out.println(airport.getName());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLatitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude(null);
    }

    @Test
    public void testFindAirportByLongitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude("-101.473911");
        assertFalse(actualAirports.isEmpty());
        for (Airport airport : actualAirports) {
            System.out.println(airport.getName());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLongitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude(null);
    }

    @Test
    public void testFindAirportByAddress() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByAddress("Stanford, US-KY");
        assertFalse(actualAirports.isEmpty());
        for (Airport airport : actualAirports) {
            System.out.println(airport.getName());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByAddressBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByAddress(null);
    }

    @Test
    public void testCountTotalAirports() {
        IAirportsApp app = AirportAppFactory.getInstance();
        assertEquals(66656, app.countTotalAirports());
    }

    @Test
    public void testFindAirportRandomly() {
        IAirportsApp app = AirportAppFactory.getInstance();
        Airport result = app.findAirportRandomly();
        System.out.println(result.getName());
        assertTrue(Airport.class.isInstance(result));
    }

    @Test
    public void testFindAirportNearMe() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> result = app.findAirportNearMe("IN-DL");
        assertFalse(result.isEmpty());
        for (Airport airport : result) {
            System.out.println(airport.getName());
        }
    }

    @Test
    public void testLogin() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testSignUp() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testHelp() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testSiteInfo() {
        fail("Not Yet Implemented ");
    }
}
