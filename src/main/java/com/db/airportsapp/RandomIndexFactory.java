package com.db.airportsapp;

import java.util.Random;

public class RandomIndexFactory {
    private static Random randomIndex;

    private RandomIndexFactory() {
        // private constructor
    }

    public static Random getInstance() {
        if (randomIndex == null) {
            // synchronized block to remove overhead
            synchronized (RandomIndexFactory.class) {
                if (randomIndex == null) {
                    // if instance is null, initialize
                    randomIndex = new Random();
                }

            }
        }
        return randomIndex;
    }
}