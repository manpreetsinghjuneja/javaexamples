package com.db.airportsapp;

public class LoginManagerFactory {
    public static ILoginManager getInstance() {
        return new LoginManagerImpl();
    }
}
